package com.dpfht.android.democonnectsocket;

import android.content.Context;

/**
 * Created by user on 2/17/18.
 */

public class SharedPreferencesHelper {

  private static final String PREFS_APP = "app";

  private static final String KEY_IP_SERVER = "ipServer";
  private static final String KEY_PORT_LIVE = "portLive";
  private static final String KEY_PORT_FILE = "portFile";
  private static final String KEY_PORT_FILE_VIDEO = "portFileVideo";
  private static final String KEY_IS_SENDING = "isSending";
  private static final String KEY_IS_CAPTURING = "isCapturing";

  public static void saveIpServer(Context context, String ipServer) {
    context.getSharedPreferences(PREFS_APP, Context.MODE_PRIVATE)
        .edit()
        .putString(KEY_IP_SERVER, ipServer)
        .apply();
  }

  public static String getIPServer(Context context) {
    return context.getSharedPreferences(PREFS_APP, Context.MODE_PRIVATE)
        .getString(KEY_IP_SERVER, "10.4.56.14");
  }

  public static void savePortServerLive(Context context, String portServerLive) {
    context.getSharedPreferences(PREFS_APP, Context.MODE_PRIVATE)
        .edit()
        .putString(KEY_PORT_LIVE, portServerLive)
        .apply();
  }

  public static String getPortServerLive(Context context) {
    return context.getSharedPreferences(PREFS_APP, Context.MODE_PRIVATE)
        .getString(KEY_PORT_LIVE, "5000");
  }

  public static void savePortServerFile(Context context, String portServerFile) {
    context.getSharedPreferences(PREFS_APP, Context.MODE_PRIVATE)
        .edit()
        .putString(KEY_PORT_FILE, portServerFile)
        .apply();
  }

  public static String getPortServerFile(Context context) {
    return context.getSharedPreferences(PREFS_APP, Context.MODE_PRIVATE)
        .getString(KEY_PORT_FILE, "5001");
  }

  public static void savePortServerFileVideo(Context context, String portServerFileVideo) {
    context.getSharedPreferences(PREFS_APP, Context.MODE_PRIVATE)
        .edit()
        .putString(KEY_PORT_FILE_VIDEO, portServerFileVideo)
        .apply();
  }

  public static String getPortServerFileVideo(Context context) {
    return context.getSharedPreferences(PREFS_APP, Context.MODE_PRIVATE)
        .getString(KEY_PORT_FILE_VIDEO, "5002");
  }

  public static void saveIsCapturing(Context context, boolean isCapturing) {
    context.getSharedPreferences(PREFS_APP, Context.MODE_PRIVATE)
        .edit()
        .putBoolean(KEY_IS_CAPTURING, isCapturing)
        .apply();
  }

  public static boolean isCapturing(Context context) {
    return context.getSharedPreferences(PREFS_APP, Context.MODE_PRIVATE)
        .getBoolean(KEY_IS_CAPTURING, false);
  }

  public static void saveIsSending(Context context, boolean isSending) {
    context.getSharedPreferences(PREFS_APP, Context.MODE_PRIVATE)
        .edit()
        .putBoolean(KEY_IS_SENDING, isSending)
        .apply();
  }

  public static boolean isSending(Context context) {
    return context.getSharedPreferences(PREFS_APP, Context.MODE_PRIVATE)
        .getBoolean(KEY_IS_SENDING, false);
  }
}
