package com.dpfht.android.democonnectsocket;

/**
 * Created by user on 2/26/18.
 */

public class AllowExecuteWrapper {

  private boolean isAllowExecute = true;

  public AllowExecuteWrapper() {
    this(true);
  }

  public AllowExecuteWrapper(boolean isAllowExecute) {
    this.isAllowExecute = isAllowExecute;
  }

  public boolean isAllowExecute() {
    return isAllowExecute;
  }

  public void setAllowExecute(boolean allowExecute) {
    isAllowExecute = allowExecute;
  }
}
