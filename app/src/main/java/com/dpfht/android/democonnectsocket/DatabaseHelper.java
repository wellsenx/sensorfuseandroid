package com.dpfht.android.democonnectsocket;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 2/19/18.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

  private static final String DATABASE_NAME = "sensor.db";
  private static final int DATABASE_VERSION = 2;

  private static final String TABLE_NAME = "tbl_sensor_data";

  public DatabaseHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase sqLiteDatabase) {
    sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS "
        + TABLE_NAME
        + "("
        + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
        + "value TEXT, "
        + "isSent INTEGER, "
        + "createdDate INTEGER"
        + ");");
  }

  @Override
  public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    onCreate(sqLiteDatabase);
  }

  //--
  public long saveValue(String value) {
    SQLiteDatabase db = getWritableDatabase();
    ContentValues contentValues = new ContentValues();
    contentValues.put("value", value);
    contentValues.put("isSent", false);

    long now = System.currentTimeMillis();

    contentValues.put("createdDate", now);

    return db.insert(TABLE_NAME, "value", contentValues);
  }

  public List<String> getAllValues() {
    List<String> values = new ArrayList<>();

    SQLiteDatabase db = getReadableDatabase();
    Cursor c = db.query(TABLE_NAME, null, null, null, null, null, null);

    if (c.moveToFirst()) {
      do {
        values.add(c.getString(c.getColumnIndex("value")));

      } while (c.moveToNext());
    }

    c.close();

    return values;
  }

  public Cursor getOldestValueNotSent() {
    SQLiteDatabase db = getReadableDatabase();
    Cursor c = db.query(TABLE_NAME, null, "isSent = ?", new String[]{"0"}, "isSent", "createdDate = min(createdDate)", "createdDate");

    return c;
  }

  public int setSentValue(long id) {
    ContentValues cv = new ContentValues();
    cv.put("isSent", true);

    return getWritableDatabase().update(TABLE_NAME, cv, "_id = ?", new String[]{String.valueOf(id)});
  }

  public int deleteAllValues() {

    SQLiteDatabase db = getWritableDatabase();

    return db.delete(TABLE_NAME, null, null);
  }
}
