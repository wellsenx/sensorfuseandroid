package com.dpfht.android.democonnectsocket;

import android.app.Application;
import android.content.Intent;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import io.fabric.sdk.android.Fabric;

/**
 * Created by user on 2/17/18.
 */

public class TheApplication extends Application {

  private static TheApplication instance;

  @Override
  public void onCreate() {
    super.onCreate();
    Fabric.with(this, new Crashlytics(), new Answers());

    instance = this;

    startStopService(true);
  }

  @Override
  public void onTerminate() {
    super.onTerminate();

    startStopService(false);

    SharedPreferencesHelper.saveIsSending(this, false);
    SharedPreferencesHelper.saveIsCapturing(this, false);
  }

  public void startStopService(boolean start) {
    /*
    Intent itn = new Intent(this, ConnectService.class);
    itn.setAction(start ? ConnectService.START_SENDING : ConnectService.STOP_SENDING);
    startService(itn);

    SharedPreferencesHelper.saveIsSending(this, start);
    */
  }

  public static TheApplication getInstance() {
    return instance;
  }
}
