package com.dpfht.android.democonnectsocket;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.location.LocationManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;
import com.dpfht.android.democonnectsocket.databinding.ActivityMainBinding;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

  public static final String ACTION_REFRESH_UI_STATE = "actionRefreshUIState";
  public static final String ACTION_CONSUME_VALUE = "actionConsumeValue";

  private static final int REQ_PERMISSION = 105;
  private static final int REQ_SETTINGS_LOCATION = 110;

  private ActivityMainBinding binding;
  private TaskSendFileToServer taskSendToServer;

  //--

  private static final int REQ_LOCATION = 200;
  private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

  private GoogleApiClient googleApiClient;
  private LocationRequest locationRequest;
  private PendingResult<LocationSettingsResult> result;
  private LocationCallback locCallback;

  //--

  private MediaRecorder recorder;
  private SurfaceHolder holder;

  private boolean recording = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    initDataBinding();
  }

  private void initDataBinding() {
    binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
  }

  private void initUIState() {
    if (SharedPreferencesHelper.isCapturing(this)) {
      binding.btnStartStop.setText(R.string.label_stop);
      binding.btnSettings.setEnabled(false);
      binding.btnSendToServer.setEnabled(false);
      binding.btnClearData.setEnabled(false);
    } else {
      binding.btnStartStop.setText(R.string.label_start);
      binding.btnSettings.setEnabled(true);
      binding.btnSendToServer.setEnabled(true);
      binding.btnClearData.setEnabled(true);
    }
  }

  public void onClickSettings(View view) {
    Intent intent = new Intent(this, SettingsActivity.class);
    startActivity(intent);
  }

  public void onClickStartStop(View view) {
    if (SharedPreferencesHelper.isCapturing(this)) {
      Intent itn = new Intent(this, ConnectService.class);
      itn.setAction(ConnectService.STOP_CAPTURING);
      startService(itn);

      recorder.setPreviewDisplay(null);
      recorder.stop();
      recording = false;

      initRecorder();
      prepareRecorder();
    } else {
      clearData();

      Intent itn = new Intent(this, ConnectService.class);
      itn.setAction(ConnectService.START_CAPTURING);
      startService(itn);

      recording = true;
      recorder.start();
    }
  }

  public void onClickSendToServer(View view) {
    DatabaseHelper databaseHelper = new DatabaseHelper(MainActivity.this);
    List<String> values = databaseHelper.getAllValues();

    if (values.size() > 0) {
      showLoading(true);

      taskSendToServer = new TaskSendFileToServer();
      taskSendToServer.execute(null, null, null);
    } else {
      Toast.makeText(MainActivity.this, "No data to send!", Toast.LENGTH_SHORT).show();
    }
  }

  public void onClickCancel(View view) {
    if (taskSendToServer != null) {
      taskSendToServer.cancel(true);
      showLoading(false);
      Toast.makeText(this, "canceled", Toast.LENGTH_LONG).show();
    }
  }

  public void onClickClearData(View view) {
    clearData();

    Toast.makeText(MainActivity.this, "Data cleared", Toast.LENGTH_SHORT).show();
  }

  private void clearData() {
    DatabaseHelper databaseHelper = new DatabaseHelper(MainActivity.this);
    databaseHelper.deleteAllValues();
    databaseHelper.close();
  }

  @Override
  protected void onResume() {
    super.onResume();

    initUIState();
    registerReceiver(refreshUIStateReceiver, new IntentFilter(ACTION_REFRESH_UI_STATE));
    registerReceiver(consumeValueReceiver, new IntentFilter(ACTION_CONSUME_VALUE));

    recorder = new MediaRecorder();

    SurfaceView surfaceView = binding.surfaceView;
    holder = surfaceView.getHolder();
    holder.addCallback(surfaceHolderCallback);
    holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
  }

  @Override
  protected void onPause() {
    super.onPause();
    unregisterReceiver(consumeValueReceiver);
    unregisterReceiver(refreshUIStateReceiver);

    //--

    Intent itn = new Intent(this, ConnectService.class);
    itn.setAction(ConnectService.STOP_CAPTURING);
    startService(itn);

    if (recorder != null) {
      if (recording) {
        recorder.stop();
        recording = false;
      }
      recorder.release();
    }
  }

  @Override
  protected void onStart() {
    if (googleApiClient != null) {
      googleApiClient.connect();
    }
    super.onStart();
  }

  @Override
  protected void onStop() {
    if (googleApiClient != null) {
      googleApiClient.disconnect();
    }
    super.onStop();
  }

  private BroadcastReceiver refreshUIStateReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      initUIState();
    }
  };

  private BroadcastReceiver consumeValueReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      doConsumeValue(intent.getStringExtra(SensorConstants.KEY_VALUE));
    }
  };

  private void doConsumeValue(String value) {
    String[] arr = value.split(",");

    if (arr[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_LOCATION)) {
      binding.tvLatitude.setText(arr[3]);
      binding.tvLongitude.setText(arr[4]);
    } else if (arr[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_ACCELEROMETER)) {
      binding.tvXAxis.setText(arr[3]);
      binding.tvYAxis.setText(arr[4]);
      binding.tvZAxis.setText(arr[5]);
    } else if (arr[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_GYROSCOPE)) {
      binding.tvXVel.setText(arr[3]);
      binding.tvYVel.setText(arr[4]);
      binding.tvZVel.setText(arr[5]);
    } else if (arr[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_PROXIMITY)) {
      binding.tvDistance.setText(arr[3]);
    } else if (arr[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_LINEAR_ACCELERATION)) {
      binding.tvX.setText(arr[3]);
      binding.tvY.setText(arr[4]);
      binding.tvZ.setText(arr[5]);
    } else if (arr[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_GRAVITY)) {
      binding.tvXGravity.setText(arr[3]);
      binding.tvYGravity.setText(arr[4]);
      binding.tvZGravity.setText(arr[5]);
    } else if (arr[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_ORIENTATION)) {
      binding.tvAzimut.setText(arr[3]);
      binding.tvPitch.setText(arr[4]);
      binding.tvRoll.setText(arr[5]);
    } else if (arr[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_BAROMETER)) {
      binding.tvPressure.setText(arr[3]);
    } else if (arr[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_THERMOMETER)) {
      binding.tvTemperature.setText(arr[3]);
    }
  }

  private synchronized void doFirstInit() {
    recorder = new MediaRecorder();

    //initRecorder();

    SurfaceView surfaceView = binding.surfaceView;
    holder = surfaceView.getHolder();
    holder.addCallback(surfaceHolderCallback);
    holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

    //prepareRecorder();
  }

  //--
  private void checkPermission() {
    if (Build.VERSION.SDK_INT >= 23) {
      if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
        ActivityCompat.requestPermissions(this, new String[] {
            Manifest.permission.ACCESS_FINE_LOCATION }, REQ_PERMISSION);
      } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_DENIED) {
        ActivityCompat.requestPermissions(this, new String[] {
                Manifest.permission.RECORD_AUDIO }, REQ_PERMISSION);
      } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
        ActivityCompat.requestPermissions(this, new String[] {
                Manifest.permission.CAMERA }, REQ_PERMISSION);
      } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
        ActivityCompat.requestPermissions(this, new String[] {
                Manifest.permission.WRITE_EXTERNAL_STORAGE }, REQ_PERMISSION);
      } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
        ActivityCompat.requestPermissions(this, new String[] {
                Manifest.permission.READ_EXTERNAL_STORAGE }, REQ_PERMISSION);
      } else {
        statusCheck();

        initRecorder();
        prepareRecorder();

      }
    } else {
      statusCheck();

      initRecorder();
      prepareRecorder();

    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    switch (requestCode) {
      case REQ_PERMISSION:
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
            grantResults[1] == PackageManager.PERMISSION_GRANTED &&
            grantResults[2] == PackageManager.PERMISSION_GRANTED &&
            grantResults[3] == PackageManager.PERMISSION_GRANTED &&
            grantResults[4] == PackageManager.PERMISSION_GRANTED) {

          AlertDialog dialog = new AlertDialog.Builder(this).create();
          dialog.setCancelable(false);
          dialog.setMessage("You need to restart this application to apply the granted permissions");
          dialog.setButton(AlertDialog.BUTTON_POSITIVE, "RESTART", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              Intent itn = new Intent(TheApplication.getInstance(), MainActivity.class);
              startActivity(itn);

              finish();
            }
          });

          dialog.show();

        } else {
          checkPermission();
        }
        break;
    }
  }

  public void statusCheck() {
    final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
      buildAlertMessageNoGps();
    } else {
      if (!checkPlayServices()) {
        return;
      }

      if (googleApiClient == null) {
        googleApiClient =
            new GoogleApiClient.Builder(MainActivity.this)
                .addConnectionCallbacks(connectionCallbacks)
                .addOnConnectionFailedListener(connectionFailedListener)
                .addApi(LocationServices.API)
                .build();
      }

      googleApiClient.connect();
    }
  }

  private void buildAlertMessageNoGps() {
    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
        .setCancelable(false)
        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
          public void onClick(final DialogInterface dialog, final int id) {
            startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQ_SETTINGS_LOCATION);
          }
        })
        .setNegativeButton("No", new DialogInterface.OnClickListener() {
          public void onClick(final DialogInterface dialog, final int id) {
            dialog.cancel();

            binding.btnStartStop.setEnabled(false);
          }
        });
    final AlertDialog alert = builder.create();
    alert.show();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    switch (requestCode) {
      case REQ_SETTINGS_LOCATION:
        statusCheck();
        break;
      case PLAY_SERVICES_RESOLUTION_REQUEST:
        statusCheck();
        break;
    }
  }

  private GoogleApiClient.ConnectionCallbacks connectionCallbacks = new GoogleApiClient.ConnectionCallbacks() {
    @Override
    public void onConnected(@Nullable Bundle bundle) {
      locationRequest = LocationRequest.create();
      locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
      locationRequest.setInterval(3 * 1000);
      locationRequest.setFastestInterval(1 * 1000);

      LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
      PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());

      checkLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
  };

  private GoogleApiClient.OnConnectionFailedListener connectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
  };

  private boolean checkPlayServices() {

    GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
    int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(this);

    if (resultCode != ConnectionResult.SUCCESS) {
      if (googleApiAvailability.isUserResolvableError(resultCode)) {
        googleApiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
      } else {
        Toast.makeText(getApplicationContext(), "This device is not supported.", Toast.LENGTH_LONG).show();
      }
      return false;
    }
    return true;
  }

  private void checkLocation() {
    LocationSettingsRequest.Builder builder =
        new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
    builder.setAlwaysShow(true);

    result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
      @Override
      public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
          case LocationSettingsStatusCodes.SUCCESS:
            getCurrentLocation();
            break;
          case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
            try {
              status.startResolutionForResult(MainActivity.this, REQ_LOCATION);
            } catch (IntentSender.SendIntentException e) {
              e.printStackTrace();
            }
            break;
          case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
            break;
        }
      }
    });
  }

  private void getCurrentLocation() {
    try {
      final FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

      locCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
          super.onLocationResult(locationResult);

          binding.tvLatitude.setText("" + locationResult.getLastLocation().getLatitude());
          binding.tvLongitude.setText("" + locationResult.getLastLocation().getLongitude());

          fusedLocationProviderClient.removeLocationUpdates(locCallback);
        }
      };

      fusedLocationProviderClient.requestLocationUpdates(LocationRequest.create(), locCallback, Looper.myLooper());

    } catch (SecurityException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void showLoading(boolean isLoading) {
    binding.flProgressBar.setVisibility(isLoading? View.VISIBLE : View.GONE);
  }

  private class TaskSendFileToServer extends AsyncTask<Void, Void, Void> {

    private ByteArrayOutputStream byteStream;

    private Socket socket;
    private BufferedOutputStream writer;

    JSONArray array = null;
    private boolean isError = false;

    public TaskSendFileToServer() {

    }

    @Override
    protected Void doInBackground(Void... voids) {
      try {
        DatabaseHelper databaseHelper = new DatabaseHelper(MainActivity.this);
        List<String> values = databaseHelper.getAllValues();
        array = new JSONArray();
        for (String value : values) {
          JSONObject object = new JSONObject();

          String[] strings = value.split(",");
          if (strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_LOCATION)) {
            try {
              object.put(SensorConstants.KEY_SENSOR_TYPE, strings[0]);
              object.put(SensorConstants.KEY_TIMESTAMP, strings[1]);
              object.put(SensorConstants.KEY_DATE, strings[2]);
              object.put(SensorConstants.KEY_LATITUDE, strings[3]);
              object.put(SensorConstants.KEY_LONGITUDE, strings[4]);
            } catch (JSONException e) {
              e.printStackTrace();
            }
          } else if (strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_ACCELEROMETER) ||
              strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_GYROSCOPE) ||
              strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_LINEAR_ACCELERATION) ||
              strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_GRAVITY)) {

            try {
              object.put(SensorConstants.KEY_SENSOR_TYPE, strings[0]);
              object.put(SensorConstants.KEY_TIMESTAMP, strings[1]);
              object.put(SensorConstants.KEY_DATE, strings[2]);
              object.put(SensorConstants.KEY_X, strings[3]);
              object.put(SensorConstants.KEY_Y, strings[4]);
              object.put(SensorConstants.KEY_Z, strings[5]);
            } catch (JSONException e) {
              e.printStackTrace();
            }

          } else if (strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_PROXIMITY)) {
            try {
              object.put(SensorConstants.KEY_SENSOR_TYPE, strings[0]);
              object.put(SensorConstants.KEY_TIMESTAMP, strings[1]);
              object.put(SensorConstants.KEY_DATE, strings[2]);
              object.put(SensorConstants.KEY_DISTANCE, strings[3]);
            } catch (JSONException e) {
              e.printStackTrace();
            }
          } else if (strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_ORIENTATION)) {
            try {
              object.put(SensorConstants.KEY_SENSOR_TYPE, strings[0]);
              object.put(SensorConstants.KEY_TIMESTAMP, strings[1]);
              object.put(SensorConstants.KEY_DATE, strings[2]);
              object.put(SensorConstants.KEY_AZIMUT, strings[3]);
              object.put(SensorConstants.KEY_PITCH, strings[4]);
              object.put(SensorConstants.KEY_ROLL, strings[5]);
            } catch (JSONException e) {
              e.printStackTrace();
            }
          } else if (strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_BAROMETER)) {
            try {
              object.put(SensorConstants.KEY_SENSOR_TYPE, strings[0]);
              object.put(SensorConstants.KEY_TIMESTAMP, strings[1]);
              object.put(SensorConstants.KEY_DATE, strings[2]);
              object.put(SensorConstants.KEY_PRESSURE, strings[3]);
            } catch (JSONException e) {
              e.printStackTrace();
            }
          } else if (strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_THERMOMETER)) {
            try {
              object.put(SensorConstants.KEY_SENSOR_TYPE, strings[0]);
              object.put(SensorConstants.KEY_TIMESTAMP, strings[1]);
              object.put(SensorConstants.KEY_DATE, strings[2]);
              object.put(SensorConstants.KEY_TEMPERATURE, strings[3]);
            } catch (JSONException e) {
              e.printStackTrace();
            }
          }

          array.put(object);
        }
        databaseHelper.close();

        byteStream = new ByteArrayOutputStream();
        byteStream.write(array.toString().getBytes());

        socket = new Socket();
        socket.connect(new InetSocketAddress(SharedPreferencesHelper.getIPServer(MainActivity.this), Integer.parseInt(SharedPreferencesHelper.getPortServerFile(MainActivity.this))), 10000);
        //socket = new Socket(SharedPreferencesHelper.getIPServer(MainActivity.this), Integer.parseInt(SharedPreferencesHelper.getPortServerFile(MainActivity.this)));
        writer = new BufferedOutputStream(socket.getOutputStream());

        writer.write(byteStream.toByteArray());
        writer.flush();

      } catch (IOException ioe) {
        ioe.printStackTrace();

        isError = true;
      } finally {
        if (writer != null) {
          try {
            writer.close();
          } catch (IOException ioe) {
          }
        }

        if (socket != null) {
          try {
            socket.close();
          } catch (IOException ioe) {
          }
        }

        if (byteStream != null) {
          try {
            byteStream.close();
          } catch (IOException ioe) {
          }
        }
      }

      try {
        File storageDir = MainActivity.this.getExternalFilesDir(Environment.DIRECTORY_MOVIES);

        if (storageDir == null) {
          ContextWrapper cw = new ContextWrapper(MainActivity.this.getApplicationContext());
          storageDir = cw.getDir("imageDir", Context.MODE_PRIVATE);
        }

        if (!storageDir.exists()) {
          storageDir.mkdirs();
        }

        File tempFile = new File(storageDir, "test1.3gp");

        FileInputStream fis = new FileInputStream(tempFile);
        byte[] bytes = new byte[1024 * 1024];
        int count = 0;
        byteStream = new ByteArrayOutputStream();

        while ((count = fis.read(bytes)) > 0) {
          byteStream.write(bytes, 0, count);
        }

        socket = new Socket();
        socket.connect(new InetSocketAddress(SharedPreferencesHelper.getIPServer(MainActivity.this), Integer.parseInt(SharedPreferencesHelper.getPortServerFileVideo(MainActivity.this))), 10000);
        //socket = new Socket(SharedPreferencesHelper.getIPServer(MainActivity.this), Integer.parseInt(SharedPreferencesHelper.getPortServerFile(MainActivity.this)));
        writer = new BufferedOutputStream(socket.getOutputStream());

        writer.write(byteStream.toByteArray());
        writer.flush();

        fis.close();
        fis = null;

      } catch (FileNotFoundException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      } finally {
        if (writer != null) {
          try {
            writer.close();
          } catch (IOException ioe) {
          }
        }

        if (socket != null) {
          try {
            socket.close();
          } catch (IOException ioe) {
          }
        }

        if (byteStream != null) {
          try {
            byteStream.close();
          } catch (IOException ioe) {
          }
        }
      }

      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);

      showLoading(false);

      if (isError) {

        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).setTitle("Error")
            .setMessage("fail to connect to server")
            .create();

        try {
          alertDialog.show();
        } catch (Exception e) {
          Toast.makeText(MainActivity.this, "fail to connect to server", Toast.LENGTH_LONG).show();
        }

      } else {
        Toast.makeText(MainActivity.this, "sent to server", Toast.LENGTH_LONG).show();
      }
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu, menu);
    return true;
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    MenuItem startStopItem = menu.findItem(R.id.startStop);
    if (SharedPreferencesHelper.isSending(this)) {
      startStopItem.setTitle(getResources().getString(R.string.label_stop_sending));
    } else {
      startStopItem.setTitle(getResources().getString(R.string.label_start_sending));
    }

    return super.onPrepareOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle item selection
    switch (item.getItemId()) {
      case R.id.startStop:
        TheApplication.getInstance().startStopService(!SharedPreferencesHelper.isSending(this));
        invalidateOptionsMenu();

        return true;

      default:
        return super.onOptionsItemSelected(item);
    }
  }

  //--

  private void initRecorder() {
    recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
    recorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);

    CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
    recorder.setProfile(cpHigh);

    File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_MOVIES);

    if (storageDir == null) {
      ContextWrapper cw = new ContextWrapper(this.getApplicationContext());
      storageDir = cw.getDir("imageDir", Context.MODE_PRIVATE);
    }

    if (!storageDir.exists()) {
      storageDir.mkdirs();
    }

    File tempFile = new File(storageDir, "test1.3gp");
    if (tempFile.exists()) {
      //tempFile.delete();
    }

    recorder.setOutputFile(tempFile.getAbsolutePath());
    //recorder.setMaxDuration(50000); // 50 seconds
    //recorder.setMaxFileSize(5000000); // Approximately 5 megabytes
  }

  private void prepareRecorder() {
    recorder.setPreviewDisplay(holder.getSurface());

    try {
      recorder.prepare();
    } catch (IllegalStateException e) {
      e.printStackTrace();
      finish();
    } catch (IOException e) {
      e.printStackTrace();
      finish();
    }
  }

  private SurfaceHolder.Callback surfaceHolderCallback = new SurfaceHolder.Callback() {
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
      if ((ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) &&
          (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) &&
          (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) &&
          (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
          (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {

        initRecorder();
        prepareRecorder();

      } else {
        checkPermission();
      }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
      if (recorder != null) {
        if (recording) {
          recorder.stop();
          recording = false;
        }
        recorder.release();
      }
    }
  };
}
