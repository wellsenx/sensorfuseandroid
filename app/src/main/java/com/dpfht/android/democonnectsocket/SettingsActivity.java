package com.dpfht.android.democonnectsocket;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import com.dpfht.android.democonnectsocket.databinding.ActivitySettingsBinding;

public class SettingsActivity extends AppCompatActivity {

  private ActivitySettingsBinding binding;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    initDataBinding();
    initUIState();
  }

  private void initDataBinding() {
    binding = DataBindingUtil.setContentView(this, R.layout.activity_settings);
  }

  private void initUIState() {
    binding.etIpServer.setText(SharedPreferencesHelper.getIPServer(this));
    binding.etPortServerLive.setText(SharedPreferencesHelper.getPortServerLive(this));
    binding.etPortServerFile.setText(SharedPreferencesHelper.getPortServerFile(this));
    binding.etPortServerFileVideo.setText(SharedPreferencesHelper.getPortServerFileVideo(this));
  }

  public void onClickCancel(View view) {
    finish();
  }

  public void onClickSave(View view) {
    EditText etIpServer = binding.etIpServer;
    String strIpServer = etIpServer.getText().toString().trim();

    if (strIpServer != null && strIpServer.length() > 0) {
      SharedPreferencesHelper.saveIpServer(this, strIpServer);
    }

    EditText etPortServerLive = binding.etPortServerLive;
    String strPortLive = etPortServerLive.getText().toString().trim();

    if (strPortLive != null && strPortLive.length() > 0) {
      SharedPreferencesHelper.savePortServerLive(this, strPortLive);
    }

    EditText etPortServerFile = binding.etPortServerFile;
    String strPortFile = etPortServerFile.getText().toString().trim();

    if (strPortFile != null && strPortFile.length() > 0) {
      SharedPreferencesHelper.savePortServerFile(this, strPortFile);
    }

    EditText etPortServerFileVideo = binding.etPortServerFileVideo;
    String strPortFileVideo = etPortServerFileVideo.getText().toString().trim();

    if (strPortFileVideo != null && strPortFileVideo.length() > 0) {
      SharedPreferencesHelper.savePortServerFileVideo(this, strPortFileVideo);
    }

    finish();
  }
}
