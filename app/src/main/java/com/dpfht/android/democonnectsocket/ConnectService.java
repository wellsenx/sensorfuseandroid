package com.dpfht.android.democonnectsocket;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by user on 2/17/18.
 */

public class ConnectService extends Service {

  private static final String TAG = ConnectService.class.getSimpleName();

  public static final String START_SENDING = "startSending";
  public static final String STOP_SENDING = "stopSending";
  public static final String START_CAPTURING = "startCapturing";
  public static final String STOP_CAPTURING = "stopCapturing";

  public static final int FOREGROUND_SERVICE_NOTIFICATION_ID = 201;

  private LocationManager locationManager = null;

  private static final int LOCATION_INTERVAL = 1000;
  private static final float LOCATION_DISTANCE = 1f;

  private SensorManager sensorManager;

  private Sensor sensorAccelerometer;
  private Sensor sensorGyroscope;
  private Sensor sensorProximity;
  private Sensor sensorLinearAcceleration;
  private Sensor sensorGravity;

  //private Sensor accelerometer;
  //private Sensor magnetometer;
  private Sensor sensorOrientation;

  private Sensor barometer;
  private Sensor thermometer;

  private AllowExecuteWrapper allowExecuteLocation;
  private AllowExecuteWrapper allowExecuteAccelerometer;
  private AllowExecuteWrapper allowExecuteGyroscope;
  private AllowExecuteWrapper allowExecuteProximity;
  private AllowExecuteWrapper allowExecuteLinearAcceleration;
  private AllowExecuteWrapper allowExecuteGravity;
  private AllowExecuteWrapper allowExecuteOrientation;
  private AllowExecuteWrapper allowExecuteBarometer;
  private AllowExecuteWrapper allowExecuteThermometer;

  private Socket socketLive;
  private BufferedOutputStream writerLive;
  private BufferedReader readerLive;

  private TaskCapturing taskCapturing;
  private TaskSending taskSending;

  //--

  //private DatabaseHelper databaseHelper;
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.ENGLISH);

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    if (intent == null) {
      return START_STICKY;
    }

    if (intent.getAction() != null && intent.getAction().equals(START_SENDING)) {
      if (taskSending != null) {
        return START_STICKY;
      }

      taskSending = new TaskSending();
      taskSending.execute(null, null, null);

    } else if (intent.getAction().equals(STOP_SENDING)) {
      taskSending.stopConsume();
      taskSending = null;

      if (!SharedPreferencesHelper.isCapturing(ConnectService.this)) {
        stopForeground(true);
        stopSelf();
      }
    } else if (intent.getAction().equals(START_CAPTURING)) {
      if (taskCapturing != null) {
        return START_STICKY;
      }

      taskCapturing = new TaskCapturing();
      taskCapturing.execute(null, null, null);

    } else if (intent.getAction().equals(STOP_CAPTURING)) {

      deactivateSensorLocation();
      deactivateSensorAccelerometer();
      deactivateSensorGyroscope();
      deactivateSensorProximity();
      deactivateSensorLinearAcceleration();
      deactivateSensorGravity();
      deactivateSensorOrientation();
      deactivateSensorBarometer();
      deactivateSensorThermometer();

      SharedPreferencesHelper.saveIsCapturing(this, false);

      if (!SharedPreferencesHelper.isSending(ConnectService.this)) {
        stopForeground(true);
        stopSelf();
      }

      taskCapturing = null;

      sendBroadcast(new Intent(MainActivity.ACTION_REFRESH_UI_STATE));
    }

    return START_STICKY;
  }

  @Override
  public void onDestroy() {
    super.onDestroy();

    SharedPreferencesHelper.saveIsSending(this, false);
    SharedPreferencesHelper.saveIsCapturing(this, false);
  }

  private void showNotification() {
    Intent notificationIntent = new Intent(this, MainActivity.class);
    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

    Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

    Notification notification = new NotificationCompat.Builder(this)
        .setContentTitle(getResources().getString(R.string.app_name))
        .setTicker(getResources().getString(R.string.app_name) + " is running")
        .setContentText("Running")
        .setSmallIcon(R.mipmap.ic_launcher)
        .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
        .setContentIntent(pendingIntent)
        .setOngoing(true)
        .build();

    startForeground(FOREGROUND_SERVICE_NOTIFICATION_ID, notification);
  }

  //--
  private LocationListener locationListener = new LocationListener() {
    @Override
    public void onLocationChanged(Location location) {
      if (allowExecuteLocation.isAllowExecute()) {
        allowExecuteLocation.setAllowExecute(false);

        StringBuffer buffer = new StringBuffer();
        buffer.append(SensorConstants.KEY_SENSOR_TYPE_LOCATION + ",");
        buffer.append(Calendar.getInstance().getTimeInMillis() + ",");
        buffer.append(dateFormat.format(Calendar.getInstance().getTimeInMillis()) + ",");
        buffer.append(location.getLatitude() + ",");
        buffer.append(location.getLongitude());

        new TaskSaveValue(buffer.toString(), allowExecuteLocation).execute(null, null, null);
      }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
      Log.d(TAG, "onStatusChanged");
    }

    @Override
    public void onProviderEnabled(String s) {
      Log.d(TAG, "onProviderEnabled");
    }

    @Override
    public void onProviderDisabled(String s) {
      Log.d(TAG, "onProviderDisabled");
    }
  };

  private void activateSensorLocation() {

    locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

    try {
      locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, locationListener);

      Log.d(TAG, "activateSensorLocation");
    } catch (java.lang.SecurityException ex) {
      Log.i(TAG, "fail to request location update, ignore", ex);
    } catch (IllegalArgumentException ex) {
      Log.d(TAG, "network provider does not exist, " + ex.getMessage());
    }
  }

  private void deactivateSensorLocation() {
    if (locationManager != null) {
      try {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
          return;
        }

        locationManager.removeUpdates(locationListener);
        locationManager = null;
      } catch (Exception ex) {
        Log.i(TAG, "fail to remove location listener, ignore", ex);
      }
    }
  }

  //--
  private SensorEventListener sensorEventListener = new SensorEventListener() {
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
      if (allowExecuteAccelerometer.isAllowExecute()) {
        allowExecuteAccelerometer.setAllowExecute(false);

        StringBuffer buffer = new StringBuffer();
        buffer.append(SensorConstants.KEY_SENSOR_TYPE_ACCELEROMETER + ",");
        buffer.append(Calendar.getInstance().getTimeInMillis() + ",");
        buffer.append(dateFormat.format(Calendar.getInstance().getTimeInMillis()) + ",");
        buffer.append(sensorEvent.values[0] + ",");
        buffer.append(sensorEvent.values[1] + ",");
        buffer.append(sensorEvent.values[2]);

        new TaskSaveValue(buffer.toString(), allowExecuteAccelerometer).execute(null, null, null);
      }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
  };

  private void activateSensorAccelerometer() {
    sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    sensorManager.registerListener(sensorEventListener, sensorAccelerometer , SensorManager.SENSOR_DELAY_NORMAL);
  }

  private void deactivateSensorAccelerometer() {
    if (sensorManager != null) {
      sensorManager.unregisterListener(sensorEventListener);

      sensorAccelerometer = null;
    }
  }

  //--

  private SensorEventListener gyroscopeSensorListener = new SensorEventListener() {
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
      if (allowExecuteGyroscope.isAllowExecute()) {
        allowExecuteGyroscope.setAllowExecute(false);

        StringBuffer buffer = new StringBuffer();
        buffer.append(SensorConstants.KEY_SENSOR_TYPE_GYROSCOPE + ",");
        buffer.append(Calendar.getInstance().getTimeInMillis() + ",");
        buffer.append(dateFormat.format(Calendar.getInstance().getTimeInMillis()) + ",");
        buffer.append(sensorEvent.values[0] + ",");
        buffer.append(sensorEvent.values[1] + ",");
        buffer.append(sensorEvent.values[2]);

        new TaskSaveValue(buffer.toString(), allowExecuteGyroscope).execute(null, null, null);
      }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
  };

  private void activateSensorGyroscope() {
    sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    sensorGyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
    sensorManager.registerListener(gyroscopeSensorListener, sensorGyroscope, SensorManager.SENSOR_DELAY_NORMAL);
  }

  private void deactivateSensorGyroscope() {
    if (sensorManager != null) {
      sensorManager.unregisterListener(gyroscopeSensorListener);

      sensorGyroscope = null;
    }
  }

  //--

  private SensorEventListener proximitySensorListener = new SensorEventListener() {
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
      if (allowExecuteProximity.isAllowExecute()) {
        allowExecuteProximity.setAllowExecute(false);

        StringBuffer buffer = new StringBuffer();
        buffer.append(SensorConstants.KEY_SENSOR_TYPE_PROXIMITY + ",");
        buffer.append(Calendar.getInstance().getTimeInMillis() + ",");
        buffer.append(dateFormat.format(Calendar.getInstance().getTimeInMillis()) + ",");
        buffer.append(sensorEvent.values[0]);

        new TaskSaveValue(buffer.toString(), allowExecuteProximity).execute(null, null, null);
      }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
  };

  private void activateSensorProximity() {
    sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    sensorProximity = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
    sensorManager.registerListener(proximitySensorListener, sensorProximity, SensorManager.SENSOR_DELAY_NORMAL);
  }

  private void deactivateSensorProximity() {
    if (sensorManager != null) {
      sensorManager.unregisterListener(proximitySensorListener);

      sensorProximity = null;
    }
  }

  //--

  private SensorEventListener linearAccelerationSensorListener = new SensorEventListener() {
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
      if (allowExecuteLinearAcceleration.isAllowExecute()) {
        allowExecuteLinearAcceleration.setAllowExecute(false);

        StringBuffer buffer = new StringBuffer();
        buffer.append(SensorConstants.KEY_SENSOR_TYPE_LINEAR_ACCELERATION + ",");
        buffer.append(Calendar.getInstance().getTimeInMillis() + ",");
        buffer.append(dateFormat.format(Calendar.getInstance().getTimeInMillis()) + ",");
        buffer.append(sensorEvent.values[0] + ",");
        buffer.append(sensorEvent.values[1] + ",");
        buffer.append(sensorEvent.values[2]);

        new TaskSaveValue(buffer.toString(), allowExecuteLinearAcceleration).execute(null, null, null);
      }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
  };

  private void activateSensorLinearAcceleration() {
    sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    sensorLinearAcceleration = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
    sensorManager.registerListener(linearAccelerationSensorListener, sensorLinearAcceleration, SensorManager.SENSOR_DELAY_NORMAL);
  }

  private void deactivateSensorLinearAcceleration() {
    if (sensorManager != null) {
      sensorManager.unregisterListener(linearAccelerationSensorListener);

      sensorLinearAcceleration = null;
    }
  }

  //--

  private SensorEventListener gravitySensorListener = new SensorEventListener() {
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
      if (allowExecuteGravity.isAllowExecute()) {
        allowExecuteGravity.setAllowExecute(false);

        StringBuffer buffer = new StringBuffer();
        buffer.append(SensorConstants.KEY_SENSOR_TYPE_GRAVITY + ",");
        buffer.append(Calendar.getInstance().getTimeInMillis() + ",");
        buffer.append(dateFormat.format(Calendar.getInstance().getTimeInMillis()) + ",");
        buffer.append(sensorEvent.values[0] + ",");
        buffer.append(sensorEvent.values[1] + ",");
        buffer.append(sensorEvent.values[2]);

        new TaskSaveValue(buffer.toString(), allowExecuteGravity).execute(null, null, null);
      }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
  };

  private void activateSensorGravity() {
    sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    sensorGravity = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
    sensorManager.registerListener(gravitySensorListener, sensorGravity, SensorManager.SENSOR_DELAY_NORMAL);
  }

  private void deactivateSensorGravity() {
    if (sensorManager != null) {
      sensorManager.unregisterListener(gravitySensorListener);

      sensorGravity = null;
    }
  }

  //--

  private SensorEventListener orientationSensorListener = new SensorEventListener() {
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

      if (allowExecuteOrientation.isAllowExecute()) {
        allowExecuteOrientation.setAllowExecute(false);

        float azimut = sensorEvent.values[0];
        float pitch = sensorEvent.values[1];
        float roll = sensorEvent.values[2];

        StringBuffer buffer = new StringBuffer();
        buffer.append(SensorConstants.KEY_SENSOR_TYPE_ORIENTATION + ",");
        buffer.append(Calendar.getInstance().getTimeInMillis() + ",");
        buffer.append(dateFormat.format(Calendar.getInstance().getTimeInMillis()) + ",");
        buffer.append(azimut + ",");
        buffer.append(pitch + ",");
        buffer.append(roll);

        new TaskSaveValue(buffer.toString(), allowExecuteOrientation).execute(null, null, null);
      }

      /*
      float[] mGravity = null;
      float[] mGeomagnetic = null;

      if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
        mGravity = sensorEvent.values;
      if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
        mGeomagnetic = sensorEvent.values;
      if (mGravity != null && mGeomagnetic != null) {
        float R[] = new float[9];
        float I[] = new float[9];
        boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
        if (success) {
          float orientation[] = new float[3];
          SensorManager.getOrientation(R, orientation);
          float azimut = orientation[0]; // orientation contains: azimut, pitch and roll
          float pitch = orientation[1];
          float roll = orientation[2];

          if (allowExecuteOrientation.isAllowExecute()) {
            allowExecuteOrientation.setAllowExecute(false);

            StringBuffer buffer = new StringBuffer();
            buffer.append(SensorConstants.KEY_SENSOR_TYPE_ORIENTATION + ",");
            buffer.append(Calendar.getInstance().getTimeInMillis() + ",");
            buffer.append(azimut + ",");
            buffer.append(pitch + ",");
            buffer.append(roll);

            new TaskSendValue(buffer.toString(), allowExecuteOrientation).execute(null, null, null);
          }
        }
      }
      */
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
  };

  private void activateSensorOrientation() {
    sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
    //accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    //magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

    //sensorManager.registerListener(orientationSensorListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    //sensorManager.registerListener(orientationSensorListener, magnetometer, SensorManager.SENSOR_DELAY_NORMAL);
    sensorOrientation = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
    sensorManager.registerListener(orientationSensorListener, sensorOrientation, SensorManager.SENSOR_DELAY_NORMAL);
  }

  private void deactivateSensorOrientation() {
    if (sensorManager != null) {
      sensorManager.unregisterListener(orientationSensorListener);

      //accelerometer = null;
      //magnetometer = null;
      sensorOrientation = null;
    }
  }

  //--

  private SensorEventListener barometerSensorListener = new SensorEventListener() {
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
      if (allowExecuteBarometer.isAllowExecute()) {
        allowExecuteBarometer.setAllowExecute(false);

        StringBuffer buffer = new StringBuffer();
        buffer.append(SensorConstants.KEY_SENSOR_TYPE_BAROMETER + ",");
        buffer.append(Calendar.getInstance().getTimeInMillis() + ",");
        buffer.append(dateFormat.format(Calendar.getInstance().getTimeInMillis()) + ",");
        buffer.append(sensorEvent.values[0]);

        new TaskSaveValue(buffer.toString(), allowExecuteBarometer).execute(null, null, null);
      }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
  };

  private void activateSensorBarometer() {
    sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    barometer = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
    sensorManager.registerListener(barometerSensorListener, barometer, SensorManager.SENSOR_DELAY_NORMAL);
  }

  private void deactivateSensorBarometer() {
    if (sensorManager != null) {
      sensorManager.unregisterListener(barometerSensorListener);

      barometer = null;
    }
  }

  //--

  private SensorEventListener thermometerSensorListener = new SensorEventListener() {
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
      if (allowExecuteThermometer.isAllowExecute()) {
        allowExecuteThermometer.setAllowExecute(false);

        StringBuffer buffer = new StringBuffer();
        buffer.append(SensorConstants.KEY_SENSOR_TYPE_THERMOMETER + ",");
        buffer.append(Calendar.getInstance().getTimeInMillis() + ",");
        buffer.append(dateFormat.format(Calendar.getInstance().getTimeInMillis()) + ",");
        buffer.append(sensorEvent.values[0]);

        new TaskSaveValue(buffer.toString(), allowExecuteThermometer).execute(null, null, null);
      }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
  };

  private void activateSensorThermometer() {
    sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    thermometer = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
    sensorManager.registerListener(thermometerSensorListener, thermometer, SensorManager.SENSOR_DELAY_NORMAL);
  }

  private void deactivateSensorThermometer() {
    if (sensorManager != null) {
      sensorManager.unregisterListener(thermometerSensorListener);

      thermometer = null;
    }
  }

  //--

  private class TaskSending extends AsyncTask<Void, Void, Void> {

    private Timer timer;

    public TaskSending() {

    }

    @Override
    protected Void doInBackground(Void... voids) {

      timer = new Timer();
      timer.schedule(new TimerTaskSendValues(), 0);

      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);

      if (!SharedPreferencesHelper.isCapturing(ConnectService.this)) {
        showNotification();
      }
    }

    public void stopConsume() {
      if (timer != null) {
        timer.cancel();
        timer = null;
      }
    }

    private class TimerTaskSendValues extends TimerTask {
      @Override
      public void run() {
        doSend();
      }

      private void doSend() {
        if (socketLive == null || socketLive.isClosed()) {
          try {
            socketLive = new Socket();
            socketLive.connect(
                new InetSocketAddress(
                    SharedPreferencesHelper.getIPServer(ConnectService.this),
                    Integer.parseInt(SharedPreferencesHelper.getPortServerLive(ConnectService.this))),
                3000);

            writerLive = new BufferedOutputStream(socketLive.getOutputStream());
            readerLive = new BufferedReader(new InputStreamReader(socketLive.getInputStream()));
          } catch (IOException e) {
            Log.i(TAG, "fail to connect to server", e);
            e.printStackTrace();

            socketLive = null;
            writerLive = null;
            readerLive = null;

            if (timer != null) {
              timer.schedule(new TimerTaskSendValues(), 3000);
            }

            return;
          }
        }

        if (socketLive != null && readerLive != null) {
          try {
            if (readerLive.ready()) {
              String str = readerLive.readLine();

              if (str.equalsIgnoreCase("CLOSE_SOCKET")) {
                readerLive.close();
                writerLive.close();
                socketLive.close();

                readerLive = null;
                writerLive = null;
                socketLive = null;

                //Log.d("DHIPA", "SOCKET_CLOSE");

                if (timer != null) {
                  timer.schedule(new TimerTaskSendValues(), 3000);
                }
              }
            }
          } catch (IOException e) {
            e.printStackTrace();
          }
        }

        if (socketLive != null && writerLive != null) {
          DatabaseHelper databaseHelper = new DatabaseHelper(ConnectService.this);
          Cursor c = databaseHelper.getOldestValueNotSent();

          if (c.moveToFirst()) {
            String value = c.getString(c.getColumnIndex("value"));

            try {
              writerLive.write((value + "\n").getBytes());
              writerLive.flush();

              databaseHelper.setSentValue(c.getLong(c.getColumnIndex("_id")));
            } catch (Exception e) {
              e.printStackTrace();
            }

            c.close();
            databaseHelper.close();

            if (timer != null) {
              timer.schedule(new TimerTaskSendValues(), 0);
            }
          } else {

            c.close();
            databaseHelper.close();

            if (timer != null) {
              timer.schedule(new TimerTaskSendValues(), 300);
            }
          }
        }
      }
    }
  }

  private class TaskCapturing extends AsyncTask<Void, Void, Void> {

    private boolean isError = false;

    public TaskCapturing() {

    }

    @Override
    protected Void doInBackground(Void... voids) {
      allowExecuteLocation = new AllowExecuteWrapper(true);
      allowExecuteAccelerometer = new AllowExecuteWrapper(true);
      allowExecuteGyroscope = new AllowExecuteWrapper(true);
      allowExecuteProximity = new AllowExecuteWrapper(true);
      allowExecuteLinearAcceleration = new AllowExecuteWrapper(true);
      allowExecuteGravity = new AllowExecuteWrapper(true);
      allowExecuteOrientation = new AllowExecuteWrapper(true);
      allowExecuteBarometer = new AllowExecuteWrapper(true);
      allowExecuteThermometer = new AllowExecuteWrapper(true);

      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);

      if (!SharedPreferencesHelper.isSending(ConnectService.this)) {
        showNotification();
      }

      activateSensorLocation();
      activateSensorAccelerometer();
      activateSensorGyroscope();
      activateSensorProximity();
      activateSensorLinearAcceleration();
      activateSensorGravity();
      activateSensorOrientation();
      activateSensorBarometer();
      activateSensorThermometer();

      SharedPreferencesHelper.saveIsCapturing(ConnectService.this, true);

      sendBroadcast(new Intent(MainActivity.ACTION_REFRESH_UI_STATE));
    }
  }

  private class TaskSaveValue extends AsyncTask<Void, Void, Void> {
    private String value;
    private AllowExecuteWrapper allowExecuteWrapper;

    public TaskSaveValue(String value, AllowExecuteWrapper allowExecuteWrapper) {
      this.value = value;
      this.allowExecuteWrapper = allowExecuteWrapper;
    }

    @Override
    protected Void doInBackground(Void... voids) {
      try {
        DatabaseHelper databaseHelper = new DatabaseHelper(ConnectService.this);
        databaseHelper.saveValue(value);
        databaseHelper.close();

        Intent intent = new Intent(MainActivity.ACTION_CONSUME_VALUE);
        intent.putExtra(SensorConstants.KEY_VALUE, value);
        sendBroadcast(intent);
      } catch (Exception e) {
        //Toast.makeText(ConnectService.this, "can't insert data", Toast.LENGTH_SHORT).show();
      } finally {
        allowExecuteWrapper.setAllowExecute(true);
      }

      return null;
    }
  }



}
