package com.dpfht.android.democonnectsocket;

/**
 * Created by user on 2/26/18.
 */

public class SensorConstants {

  public static final String KEY_VALUE = "value";

  public static final String KEY_SENSOR_TYPE_LOCATION = "location";
  public static final String KEY_SENSOR_TYPE_ACCELEROMETER = "accelerometer";
  public static final String KEY_SENSOR_TYPE_GYROSCOPE = "gyroscope";
  public static final String KEY_SENSOR_TYPE_PROXIMITY = "proximity";
  public static final String KEY_SENSOR_TYPE_LINEAR_ACCELERATION = "linearAcceleration";
  public static final String KEY_SENSOR_TYPE_GRAVITY = "gravity";
  public static final String KEY_SENSOR_TYPE_ORIENTATION = "orientation";
  public static final String KEY_SENSOR_TYPE_BAROMETER = "barometer";
  public static final String KEY_SENSOR_TYPE_THERMOMETER = "thermometer";

  public static final String KEY_SENSOR_TYPE = "sensorType";
  public static final String KEY_TIMESTAMP = "timestamp";
  public static final String KEY_DATE = "date";
  public static final String KEY_LATITUDE = "latitude";
  public static final String KEY_LONGITUDE = "longitude";
  public static final String KEY_X = "x";
  public static final String KEY_Y = "y";
  public static final String KEY_Z = "z";
  public static final String KEY_DISTANCE = "distance";
  public static final String KEY_AZIMUT = "azimut";
  public static final String KEY_PITCH = "pitch";
  public static final String KEY_ROLL = "roll";
  public static final String KEY_PRESSURE = "pressure";
  public static final String KEY_TEMPERATURE = "temperature";
}
